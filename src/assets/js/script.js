/**
 * Toggles the choice of the theme of the site.
 *
 * @param {boolean} enabled Force theme selection
 *
 * @returns void
 */
const toggleDarkMode = (enabled) => {
  const $html = document.querySelector('html');
  const $favicon = document.querySelector("link[rel~='icon']");
  let enabledDarkMode = false;

  if (
    enabled === true
    || (!$html.classList.contains('dark') && enabled !== false)
  ) {
    enabledDarkMode = true;
  }

  if (enabledDarkMode) {
    $html.classList.add('dark');
    $favicon.href = './assets/img/favicon-dark.svg';
  } else {
    $html.classList.remove('dark');
    $favicon.href = './assets/img/favicon-light.svg';
  }
};

/**
 * Executed after the page is loaded.
 */
window.onload = () => {
  // Detects the default theme of the browser
  if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
    toggleDarkMode(true);
  }

  // Adds a listener to detect when the browser theme changes
  window
    .matchMedia('(prefers-color-scheme: dark)')
    .addEventListener('change', (e) => {
      if (e.matches) {
        toggleDarkMode(true);
      } else {
        toggleDarkMode(false);
      }
    });

  // Updates the copyright year
  const year = new Date().getFullYear();
  document.querySelectorAll('.footer__copyright > span').forEach(($date) => {
    // eslint-disable-next-line no-param-reassign
    $date.innerHTML = year;
  });
};

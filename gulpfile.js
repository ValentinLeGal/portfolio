// plugins
const gulp = require('gulp');
const del = require('del');
const htmlmin = require('gulp-htmlmin');
const minifyCss = require('gulp-clean-css');
const plumber = require('gulp-plumber');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass')(require('node-sass'));
const jsmin = require('gulp-jsmin');
const browsersync = require('browser-sync').create();

const publicFolder = './public/';
const srcFolder = './src/';

const paths = {
  css: {
    src: './src/assets/css/**/*.css',
    dest: './public/assets/css/',
  },
  js: {
    src: './src/assets/js/**/*.js',
    dest: './public/assets/js/',
  },
  scss: {
    src: './src/assets/css/**/*.scss',
    dest: './public/assets/css/',
  },
  html: {
    src: './src/**/*.html',
    dest: './public/',
  },
  img: {
    src: './src/assets/img/**/*',
    dest: './public/assets/img/',
  },
  font: {
    src: './src/assets/font/**/*',
    dest: './public/assets/font/',
  },
  robots: {
    src: './src/robots.txt',
    dest: './public/',
  },
  sitemap: {
    src: './src/sitemap.xml',
    dest: './public/',
  },
};

function browserSyncDev() {
  browsersync.init({
    server: {
      baseDir: srcFolder,
    },
    port: 3000,
  });
}

// function browserSync() {
//   browsersync.init({
//     server: {
//       baseDir: publicFolder,
//     },
//     port: 3000,
//   });
// }

// Clean public
function clear() {
  return del([publicFolder]);
}

function html() {
  return gulp
    .src(paths.html.src, { since: gulp.lastRun(html) })
    .pipe(plumber())
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest(paths.html.dest))
    .pipe(browsersync.stream());
}

function css() {
  return gulp
    .src(paths.css.src, { since: gulp.lastRun(css) })
    .pipe(plumber())
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(gulp.dest(paths.css.dest))
    .pipe(browsersync.stream());
}

function js() {
  return gulp
    .src(paths.js.src, { since: gulp.lastRun(js) })
    .pipe(plumber())
    .pipe(jsmin())
    .pipe(gulp.dest(paths.js.dest))
    .pipe(browsersync.stream());
}

function scss() {
  return gulp
    .src(paths.scss.src, { since: gulp.lastRun(scss) })
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(gulp.dest(paths.scss.dest))
    .pipe(browsersync.stream());
}

function scssDev() {
  return gulp
    .src(paths.scss.src, { since: gulp.lastRun(scssDev) })
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(minifyCss())
    .pipe(gulp.dest('./src/assets/css/'))
    .pipe(browsersync.stream());
}

function images() {
  return gulp
    .src(paths.img.src)
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest(paths.img.dest))
    .pipe(browsersync.stream());
}

// Listen to what is happening on paths.css.src, and run the css function if there are changes
// function watchFiles() {
//   gulp.watch(paths.scss.src, scss);
//   gulp.watch(paths.js.src, js);
//   gulp.watch(paths.css.src, css);
//   gulp.watch(paths.html.src, html);
//   gulp.watch(paths.img.src, images);
// }

function watch() {
  gulp.watch(paths.scss.src, scssDev);
  gulp
    .watch([paths.css.src, paths.js.src, paths.html.src])
    .on('change', browsersync.reload);
}

function font() {
  return gulp
    .src(paths.font.src, { since: gulp.lastRun(font) })
    .pipe(plumber())
    .pipe(gulp.dest(paths.font.dest))
    .pipe(browsersync.stream());
}

function robots() {
  return gulp
    .src(paths.robots.src, { since: gulp.lastRun(robots) })
    .pipe(gulp.dest(paths.robots.dest));
}

function sitemap() {
  return gulp
    .src(paths.sitemap.src, { since: gulp.lastRun(sitemap) })
    .pipe(gulp.dest(paths.sitemap.dest));
}

const serie = gulp.series(
  clear,
  html,
  js,
  scss,
  css,
  images,
  font,
  robots,
  sitemap,
);

const build = gulp.series(serie);

const dev = gulp.parallel(watch, browserSyncDev);

// Exports
exports.clear = clear;
exports.build = build;
exports.dev = dev;
exports.default = dev;
